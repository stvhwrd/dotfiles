#!/usr/bin/env bash

# Install command-line tools using Homebrew.

# Ask for the administrator password upfront.
sudo -v

# Keep-alive: update existing `sudo` time stamp until the script has finished.
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

# Make sure we’re using the latest Homebrew.
brew update

# Upgrade any already-installed formulae.
brew upgrade

# Install Homebrew Cask for extra awesomeness
brew install caskroom/cask/brew-cask

# Add the Caskroom for fonts
brew tap caskroom/fonts 

# Update to include the new caskroom packages.
brew update

# Install GNU core utilities (those that come with OS X are outdated).
# Don’t forget to add `$(brew --prefix coreutils)/libexec/gnubin` to `$PATH`.
brew install coreutils
sudo ln -s /usr/local/bin/gsha256sum /usr/local/bin/sha256sum

# Install some other useful utilities like `sponge`.
brew install moreutils
# Install GNU `find`, `locate`, `updatedb`, and `xargs`, `g`-prefixed.
brew install findutils
# Install GNU `sed`, overwriting the built-in `sed`.
brew install gnu-sed --with-default-names
# Install Bash 4.
# Note: don’t forget to add `/usr/local/bin/bash` to `/etc/shells` before
# running `chsh`.
#brew install bash
#brew install bash-completion

# Install `wget` with IRI support.
brew install wget --with-iri

# Install RingoJS and Narwhal.
# Note that the order in which these are installed is important;
# see http://git.io/brew-narwhal-ringo.
#brew install ringojs
#brew install narwhal

# Install more recent versions of some OS X tools.
brew install vim --override-system-vi
brew install homebrew/dupes/grep
brew install homebrew/dupes/openssh
brew install homebrew/dupes/screen
#brew install homebrew/php/php55 --with-gmp

# Install font tools.
#brew tap bramstein/webfonttools
#brew install sfnt2woff
#brew install sfnt2woff-zopfli
#brew install woff2

# Install some CTF tools; see https://github.com/ctfs/write-ups.
#brew install bfg
#brew install binutils
#brew install binwalk
#brew install cifer
#brew install dex2jar
#brew install dns2tcp
#brew install fcrackzip
#brew install foremost
#brew install hashpump
#brew install hydra
#brew install john
#brew install knock
#brew install nmap
#brew install pngcheck
#brew install socat
#brew install sqlmap
#brew install tcpflow
#brew install tcpreplay
#brew install tcptrace
#brew install ucspi-tcp # `tcpserver` etc.
#brew install xpdf
#brew install xz

# Install other useful binaries.
brew install ack
brew install bro
#brew install exiv2
brew install git
#brew install imagemagick --with-webp
#brew install lua
#brew install lynx
#brew install p7zip
#brew install pigz
#brew install pv
#brew install rename
#brew install rhino
brew install speedtest_cli
#brew install tree
#brew install webkit2png
#brew install zopfli

# Install the binaries I usually end up with:
brew install colordiff
brew install duck
brew install fish
brew install htop-osx

# Install Node.js. Note: this installs `npm` too, using the recommended
# installation method.
#brew install node

# Install some Casks!
#brew cask install flux
#brew cask install tunnelblick
#brew cask install google-chrome
#brew cask install vlc
#brew cask install appcleaner
#brew cask install cdock
#brew cask install cyberduck
#brew cask install iterm2
#brew cask install musicbrainz picard
#brew cask install paintbrush
#brew cask install transmission
#brew cask install trim enabler
#brew cask install xld
#brew cask install liteicon
#brew cask install coconutbattery
#brew cask install mplayerx
brew cask install font-inconsolata-dz

# RUBY - Install the ever-cool Bro Pages (http://bropages.org)
gem install bropages

# Remove outdated versions from the cellar.
brew cleanup
